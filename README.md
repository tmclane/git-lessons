# Learning Git

## Initial Git Configuration

The following git configuration is not required although it is the author's belief
that it will make your use of git on the command-line easier and much more pleasant.

It is especially helpful so as to avoid remembering the `log --graph` syntax.

Place the following in your .gitconfig configuration file.

Note:  This file is found in different locations depending on your OS of choice.

- Linux / OS X: ~/.gitconfig
- Windows: The .gitconfig file is found in the $HOME directory (C:\Users\$USER for most people).


.gitconfig

        [user]
        	name = Your name here. ex: John Smith
        	email = john.smith@zimbra.com

        [color]
        	diff = auto
        	status = auto
        	branch = auto

        [alias]
            st = status
            ci = commit
            br = branch
            co = checkout
            cp = cherry-pick
            df = diff
            dc = diff --cached
            lg = log -p
            lol = log --graph --decorate --pretty=oneline --abbrev-commit
            lola = log --graph --decorate --pretty=oneline --abbrev-commit --all
            lala = log --graph --decorate --pretty='format: %C(yellow)%h%Creset [%an]%Cred%d %Cgreen%s' --abbrev-commit --all

            ls = ls-files

            # Show files ignored by git:
            ign = ls-files -o -i --exclude-standard

## Repository Structure

This repository is made up of submodules which are themselves git repositories that
contain a particular exercise.

Prepare your clone for working through the exercises by doing the following:

    # git clone git@github.com:zimbra/git-lessons.git

    Cloning into 'git-lessons'...
    remote: Counting objects: 24, done.
    remote: Compressing objects: 100% (23/23), done.
    remote: Total 24 (delta 6), reused 0 (delta 0)
    Receiving objects: 100% (24/24), done.
    Resolving deltas: 100% (6/6), done.
    Checking connectivity... done.

Enter the directory and update the submodules.

    # cd git-lessons
    # git submodule update --init

    Submodule path 'rebase-merge-feature': checked out '2ed50302e59f72d8e62daec0d8b3a78aa11a9267'

## Working through the Lessons

Each lesson is a git repository in it's own right.
If you do not see the following directories in the `git-lessons` directory please see the
`submodule update` step in the `Repository Structure` section immediately preceding this section.
